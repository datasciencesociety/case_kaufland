import pandas as pd
import numpy as np
import random as rnd


# machine learning
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPRegressor
from sklearn import metrics

everything = pd.read_csv('TSD.csv')
# train_df = pd.read_csv('1dif_joined_2016_2145.csv').head(1000)
# test_df = pd.read_csv('1dif_joined_2017_2145.csv').head(300)
train_df = everything.head(800)
test_df = everything.tail(200)

all = [train_df, test_df]

def mapTown(x, data):
    return data.loc[data['store_town'] == x].index

# Fill missing billa stores and
for data in all:
    # data['billa_stores'] = pd.to_numeric(data['billa_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['fantastico_stores'] = pd.to_numeric(data['fantastico_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['metro_stores'] = pd.to_numeric(data['metro_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['other_stores'] = pd.to_numeric(data['other_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['lidl_stores'] = pd.to_numeric(data['lidl_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['cba_stores'] = pd.to_numeric(data['cba_stores'], errors='coerce').fillna(0).astype(np.int64)
    # data['t-market_stores'] = pd.to_numeric(data['t-market_stores'], errors='coerce').fillna(0).astype(np.int64)

    # del data['country']
    # del data['date_store_closing']
    # del data['date_store_opening']
    # del data['date_imbiss_opening']
    # del data['store_name1']
    # del data['store_type']
    # data['store_town'].apply(lambda x: mapTown(x, data))
    # data['store_town'] = pd.to_numeric(data['store_town'], errors='coerce').fillna(0).astype(np.int64)
    # del data['Name']
    data['date'] = pd.to_datetime(data['date'])
    data['date_number'] = (data['date'] - data['date'].min()).dt.days
    data['1dif'] = data['number_customer'] - data['number_customer'].shift(-1)
    data['1dif'] = data['1dif'].fillna(0)
    del data['date']
    del data['number_customer']
    # del data['store_id']
    # del data['1dif']
    # data['population'] = pd.to_numeric(data['population'], errors='coerce').fillna(0).astype(np.float64)

# Print info and data example
# train_df.info()
train_df.tail()

# Obtain target variable and remove from trainging/test sets
target_col = '1dif' # number_customer or 1dif
y = train_df[target_col]
test_y = test_df[target_col]
del test_df[target_col]
del train_df[target_col]






regression = LinearRegression()
regression.fit(train_df, y)


# NEURAL NETWORK
nn = MLPRegressor(solver='adam', alpha=1e-10,
                   hidden_layer_sizes=(len(train_df.columns) + 1, len(train_df.columns) + 1, len(train_df.columns) + 1), random_state=1)
nn.fit(train_df, y)

# Random Forest regressors
rfr = RandomForestRegressor(n_estimators=160)
rfr = rfr.fit(train_df, y)

predict_regression = regression.predict(test_df)
predict_nn = nn.predict(test_df)
# predict_svm = svm.predict(test_df)
predict_rfr = rfr.predict(test_df)
# predict_dt = dt.predict(test_df)
predict_y = predict_rfr
explained_variance = metrics.explained_variance_score(test_y, predict_y)
r2_score = metrics.r2_score(test_y, predict_y)
print explained_variance
print r2_score
n = len(train_df.axes[0])
p = len(train_df.columns)
print (n, p)
r2_adjusted = 1 - ((1-r2_score)*(float((n-1))/(n-p-1)))
print (r2_score, r2_adjusted)
# (explained_variance, r2_score, r2_adjusted, regression.coef_)
zip(regression.coef_, train_df.columns)